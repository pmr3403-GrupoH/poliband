#ifndef PULSESENSOR_C
#define PULSESENSOR_C

#include <Arduino.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2);

// Variaveis
int pulseValue; // armazena o valor analógico do sensor
int bpm; // batimentos por minuto
int count = 0; // contador para filtrar o valor de bpm
int sum_bpm = 0; // soma de valores de bpm medidos
int bpmShow; // armazena o valor de bpm a ser mostrado

// Transforma o sinal analógico em bpm e filtra os resultados
void pulseSensorBPM() {

  // Detecta um pulso
  if (pulseValue > 400) {

    bpm = 60000 / pulseValue + 45; // calcula a freq. cardiaca em bpm

    // Filtra os picos
    if (bpm <130){
      if (bpm !=86){ // 86 é o valor normal sem contato no sensor

        if (bpm>100){ // Caso haja um pequeno pico
          // Atualiza o valor de bpm no display
          bpmShow = bpm;
         // lcd.setCursor(12, 0);
         // lcd.print(bpmShow);
          //lcd.print(" ");
        }
        // Soma 5 valores de bpm
        sum_bpm += bpm;
        count+=1;
      }
    //Faz a média de 5 valores
    if (count == 5){

      bpmShow = sum_bpm/5;
      count = 0; // zera o contador
      sum_bpm = 0; // zera a soma
      
      // Atualiza o valor de bpm no display
      //lcd.setCursor(12, 0);
      //lcd.print(bpmShow);
      //lcd.print(" ");
    }
  }
    delay(400);
  }
}

#endif

void freq_Display(){
  // Coloca batimentos no Display
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Batimentos:");
  lcd.setCursor(12, 0);
  lcd.print(bpmShow);
  lcd.print(" ");
}
