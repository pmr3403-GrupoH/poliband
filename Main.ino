#define BLYNK_TEMPLATE_ID "TMPL2w1YjP1PH"
#define BLYNK_TEMPLATE_NAME "POLIBAND"
#define BLYNK_AUTH_TOKEN "dx7BkBXm5f5uLY7zrpFN7Q-e6P7bmalC"

#define BLYNK_PRINT Serial

#define true 1
#define false 0

#define NUM_ESTADOS 6
#define NUM_EVENTOS 3

// ESTADOS
#define Display_horario 0
#define Display_freq 1
#define Display_passos 2
#define Display_calorias 3
#define Iniciado_atv 4
#define Pausado_atv 5

// EVENTOS
#define NENHUM_EVENTO -1
#define Bot1 0
#define Bot2 1
#define Bot3 2

// ACOES
#define NENHUMA_ACAO -1
#define A01 0
#define A02 1
#define A03 2
#define A04 3
#define A05 4
#define A06 5
#define A07 6

#include <BlynkSimpleEsp32.h>
#include "displayHorario.h"
#include "pulseSensor.h"
#include "contador_passos.h"


// Your WiFi credentials.
// Set password to "" for open networks.
char auth[] = BLYNK_AUTH_TOKEN;
char ssid[] = "Caio";
char pass[] = "caio1234";
BlynkTimer timer;

int codigoEvento = NENHUM_EVENTO;
int eventoInterno = NENHUM_EVENTO;
int estado = Display_horario;
int codigoAcao = A05;
int acao_matrizTransicaoEstados[NUM_ESTADOS][NUM_EVENTOS];
int proximo_estado_matrizTransicaoEstados[NUM_ESTADOS][NUM_EVENTOS];
//EventRow::Event event;
//int aux;+

// Definição de Pinos
const int pulsePin = 32; // o pino do sensor de freq cardíaca

//DINÂMICA DE BOTÕES
//Dinâmica botão 1
int Bot1_pin = 5;
int botao_Bot1_apertado = false;
int lastButtonState1 = LOW;

int B1_update(void) {
  botao_Bot1_apertado = digitalRead(Bot1_pin); // HIGH ao apertar
  if (lastButtonState1 != botao_Bot1_apertado) { // state changed
    //delay(50); // debounce time
    botao_Bot1_apertado = digitalRead(Bot1_pin); // HIGH ao apertar
    LCD.clear();
    delay(1000);
  }
  lastButtonState1 = botao_Bot1_apertado;
  return botao_Bot1_apertado;
}

//Dinâmica botão 2
int botao_Bot2_apertado;
int Bot2_pin = 18;
int lastButtonState2;

int B2_update(void) {
  if(estado == 2 || estado == 4 || estado == 5){
    botao_Bot2_apertado = digitalRead(Bot2_pin); // HIGH ao apertar
    if (lastButtonState2 != botao_Bot2_apertado) { // state changed
      //delay(50); // debounce time
      botao_Bot2_apertado = digitalRead(Bot2_pin); // HIGH ao apertar
      LCD.clear();
      delay(1000);
    }
    lastButtonState2 = botao_Bot2_apertado;
    return botao_Bot2_apertado;
  }
}

//Dinâmica botão 3
int botao_Bot3_apertado;
int Bot3_pin = 19;
int lastButtonState3;

int B3_update(void) {
  if(estado == 5){
    botao_Bot3_apertado = digitalRead(Bot3_pin); // HIGH ao apertar
    if (lastButtonState3 != botao_Bot3_apertado) { // state changed
      //delay(50); // debounce time
      botao_Bot3_apertado = digitalRead(Bot3_pin); // HIGH ao apertar
      LCD.clear();
      delay(1000);
    }
    lastButtonState3 = botao_Bot3_apertado;
    return botao_Bot3_apertado;
  }
}

int executarAcao(int codigoAcao)
{
  int retval;

  retval = NENHUM_EVENTO;
  if (codigoAcao == NENHUMA_ACAO)
    return retval;

  switch (codigoAcao)
  {
    case A01: // Display de frequência cardíaca em batimentos por minuto
        Serial.println("Mostrar no display freq cardiaca");

        contador_passos(); //conta os passos e as calorias
        Blynk.virtualWrite(V1, stepCount); //envia os passos atuais ao aplicativo
        Blynk.virtualWrite(V2, caloriaShow); //envia as calorias atuais ao aplicativo
        
        pulseValue = analogRead(pulsePin); //lê o valor analógico no pino do sensor de freq. cardiaca
        freq_Display(); //mostra a frequência cardíaca no display
        pulseSensorBPM(); //faz a amostragem de batimentos por minuto
        Blynk.virtualWrite(V0, bpmShow); //envia os batimentos atuais ao aplicativo

        break;

    case A02: // Display de passos
        Serial.println("Mostrar no display quantidade de passos");

        contador_passos(); //conta os passos e as calorias
        pulseSensorBPM();
        Blynk.virtualWrite(V0, bpmShow); //envia os batimentos atuais ao aplicativo
        Blynk.virtualWrite(V1, stepCount); //envia os passos atuais ao aplicativo
        Blynk.virtualWrite(V2, caloriaShow); //envia as calorias atuais ao aplicativo
        Display_steps(); // mostra os passos no Display

        break;

    case A03: // Display de calorias
        Serial.println("Mostrar no display quantidade de calorias");

        Blynk.virtualWrite(V0, bpmShow); //envia os batimentos atuais ao aplicativo
        pulseSensorBPM();

        contador_passos(); // conta os passos e as calorias
        Blynk.virtualWrite(V1, stepCount); // envia os passos atuais ao aplicativo

        Display_calories(); // mostra as calorias no Display
        Blynk.virtualWrite(V2, caloriaShow); // envia as calorias atuais ao aplicativo

        break;
        
    case A04: // Inicia Atividade
        Serial.println("Inicia contagem de passo/calorias e mantém display de passos");

        Blynk.virtualWrite(V0, bpmShow); //envia os batimentos atuais ao aplicativo
        pulseSensorBPM();

        CONTADORF = 1;
        contador_passos(); //conta os passos e as calorias
        Blynk.virtualWrite(V1, stepCount); // envia os passos atuais ao aplicativo
        Blynk.virtualWrite(V2, caloriaShow); // envia as calorias atuais ao aplicativo
        Display_steps(); // mostra os passos no Display

        break;

    case A05: // Display de horário

        Serial.println("Mostra no display o horario");

        Blynk.virtualWrite(V0, bpmShow); //envia os batimentos atuais ao aplicativo
        pulseSensorBPM();

        printLocalTime();
        contador_passos(); //conta os passos e as calorias
        Blynk.virtualWrite(V1, stepCount); // envia os passos atuais ao aplicativo
        Blynk.virtualWrite(V2, caloriaShow ); // envia as calorias atuais ao aplicativo

        break;

    case A06: //Pausar a atividade

        Serial.println("Pausa atividade, mantém display de passos.");

        Blynk.virtualWrite(V0, bpmShow); //envia os batimentos atuais ao aplicativo
        pulseSensorBPM();

        Display_steps(); // mostra os passos no Display
        CONTADORF = 0;

        Blynk.virtualWrite(V1, stepCount); // envia os passos atuais ao aplicativo
        Blynk.virtualWrite(V2, caloriaShow); // envia as calorias atuais ao aplicativo

        break;

    case A07: // Reset a atividade
        Serial.println("Reset e mantém no display de passos");

        Blynk.virtualWrite(V0, bpmShow); //envia os batimentos atuais ao aplicativo
        pulseSensorBPM();

        stepCount = 0;
        Display_steps(); // mostra os passos no Display

        Blynk.virtualWrite(V1, stepCount); // envia os passos atuais ao aplicativo
        Blynk.virtualWrite(V2, caloriaShow); // envia as calorias atuais ao aplicativo

        break;

  } // switch

  return retval;
} // executarAcao

void iniciaMaquinaEstados()
{
  int i;
  int j;

  for (i = 0; i < NUM_ESTADOS; i++)
  {
    for (j = 0; j < NUM_EVENTOS; j++)
    {
      acao_matrizTransicaoEstados[i][j] = NENHUMA_ACAO;
      proximo_estado_matrizTransicaoEstados[i][j] = i;
    }
  }
  proximo_estado_matrizTransicaoEstados[Display_horario][Bot1] = Display_freq;
  acao_matrizTransicaoEstados[Display_horario][Bot1] = A01;

  proximo_estado_matrizTransicaoEstados[Display_horario][Bot2] = Display_horario;
  acao_matrizTransicaoEstados[Display_horario][Bot2] = NENHUMA_ACAO;

  proximo_estado_matrizTransicaoEstados[Display_horario][Bot3] = Display_horario;
  acao_matrizTransicaoEstados[Display_horario][Bot3] = NENHUMA_ACAO;

  proximo_estado_matrizTransicaoEstados[Display_freq][Bot1] = Display_passos;
  acao_matrizTransicaoEstados[Display_freq][Bot1] = A02;

  proximo_estado_matrizTransicaoEstados[Display_freq][Bot2] = Display_freq;
  acao_matrizTransicaoEstados[Display_freq][Bot2] = NENHUMA_ACAO;

  proximo_estado_matrizTransicaoEstados[Display_freq][Bot3] = Display_freq;
  acao_matrizTransicaoEstados[Display_freq][Bot3] = NENHUMA_ACAO;

  proximo_estado_matrizTransicaoEstados[Display_passos][Bot1] = Display_calorias;
  acao_matrizTransicaoEstados[Display_passos][Bot1] = A03;

  proximo_estado_matrizTransicaoEstados[Display_passos][Bot2] = Iniciado_atv;
  acao_matrizTransicaoEstados[Display_passos][Bot2] = A04;

  proximo_estado_matrizTransicaoEstados[Display_passos][Bot3] = Display_passos;
  acao_matrizTransicaoEstados[Display_passos][Bot3] = NENHUMA_ACAO;

  proximo_estado_matrizTransicaoEstados[Display_calorias][Bot1] = Display_horario;
  acao_matrizTransicaoEstados[Display_calorias][Bot1] = A05;

  proximo_estado_matrizTransicaoEstados[Display_calorias][Bot2] = Display_calorias;
  acao_matrizTransicaoEstados[Display_calorias][Bot2] = NENHUMA_ACAO;

  proximo_estado_matrizTransicaoEstados[Display_calorias][Bot3] = Display_calorias;
  acao_matrizTransicaoEstados[Display_calorias][Bot3] = NENHUMA_ACAO;

  proximo_estado_matrizTransicaoEstados[Iniciado_atv][Bot1] = Display_calorias;
  acao_matrizTransicaoEstados[Iniciado_atv][Bot1] = A03;

  proximo_estado_matrizTransicaoEstados[Iniciado_atv][Bot2] = Pausado_atv;
  acao_matrizTransicaoEstados[Iniciado_atv][Bot2] = A06;

  proximo_estado_matrizTransicaoEstados[Iniciado_atv][Bot3] = Iniciado_atv;
  acao_matrizTransicaoEstados[Iniciado_atv][Bot3] = NENHUMA_ACAO;

  proximo_estado_matrizTransicaoEstados[Pausado_atv][Bot1] = Display_calorias;
  acao_matrizTransicaoEstados[Pausado_atv][Bot1] = A03;

  proximo_estado_matrizTransicaoEstados[Pausado_atv][Bot2] = Iniciado_atv;
  acao_matrizTransicaoEstados[Pausado_atv][Bot2] = A04;

  proximo_estado_matrizTransicaoEstados[Pausado_atv][Bot3] = Display_passos;
  acao_matrizTransicaoEstados[Pausado_atv][Bot3] = A07;

} // initStateMachine

int obterEvento()
{
  int retval = NENHUM_EVENTO;
  //teclas = ihm.obterTeclas();
  if (botao_Bot1_apertado) {
        return Bot1;
  }
  else if (botao_Bot2_apertado) {
        return Bot2;
  }
  else if (botao_Bot3_apertado && estado == 5) {
        return Bot3;
  }
  else {
        return NENHUM_EVENTO;
  }

  return retval;

} // obterEvento

int obterAcao(int estado, int codigoEvento)
{
  return acao_matrizTransicaoEstados[estado][codigoEvento];
} // obterAcao

int obterProximoEstado(int estado, int codigoEvento)
{
  return proximo_estado_matrizTransicaoEstados[estado][codigoEvento];
} // obterAcao

void setup(){

  Serial.begin(115200);  // inicia serial
  iniciaMaquinaEstados();
  timeInit();
  pinMode(pulsePin, INPUT);
  contador_passos_init();
  Blynk.begin(auth, ssid, pass);
  while (!Blynk.connected()) {
    Serial.println("Connecting to Blynk...");
    delay(1000);
  Serial.println("Connected to Blynk");
  }
  
}

void loop(){

  Serial.println(estado);

  B1_update();
  B2_update();
  B3_update();

  codigoEvento = obterEvento();
    if (codigoEvento != NENHUM_EVENTO)
    {
      codigoAcao = obterAcao(estado, codigoEvento);
      eventoInterno = executarAcao(codigoAcao);
      estado = obterProximoEstado(estado, codigoEvento);
    }

    executarAcao(codigoAcao);
  Blynk.run();
}
