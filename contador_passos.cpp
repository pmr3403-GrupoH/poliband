#ifndef CONTADOR_PASSOS_C
#define CONTADOR_PASSOS_C

#include <Wire.h>                 // Must include Wire library for I2C
#include "SparkFun_MMA8452Q.h"    // Click here to get the library: http://librarymanager/All#SparkFun_MMA8452Q   
#include <LiquidCrystal_I2C.h>

#include "contador_passos.h"

MMA8452Q accel;                      // create instance of the MMA8452 class
LiquidCrystal_I2C LCDA(0x27, 16, 2);  // Endereço I2C e tamanho do display LCD

float threshold = 1.3;      // Valor maximo do modulo da aceleracao para deteccao de passos
int stepCount = 0;          // contador de passos
int caloriaShow;
float caloriasPorPasso = 0.04;
float caloriasGastas;

float samples[windowSize];
int currentIndex = 0;

int CONTADORF = 0;

// Inicializa o contador de passos
// void setup(){
//   Serial.begin(9600);
//   Wire.begin();
//   Wire.begin(4, 5);
void contador_passos_init() {
  // Inicializa o acelerometro
  accel.init();
  // Checa conexao do acelerometro 
  if (accel.begin() == false) {
    //Serial.println("Not Connected. Please check connections and read the hookup guide.");
    //while (1);
  }
}
//void loop(){
void contador_passos() {
  if(CONTADORF){
    float x = accel.getCalculatedX();  // aceleracao no eixo x
    float y = accel.getCalculatedY();  // aceleracao no eixo y
    float z = accel.getCalculatedZ();  // aceleracao no eixo z

    // Calcula a magnitude do vetor de aceleracao
    float magnitude = sqrt(x * x + y * y + z * z);

    // Adiciona a amostra atual ao buffer de amostras
    samples[currentIndex] = magnitude;
    currentIndex = (currentIndex + 1) % windowSize;
    
    // Verifica se houve um pico de aceleracao
    if (isStep()) {
      stepCount++;
      caloriasGastas = caloriasPorPasso*stepCount;
      caloriaShow = caloriasGastas;
    }
    delay(100);
  }
}

// Funcao que mostra os passos no LCD
void Display_steps(){
  if(CONTADORF){
    LCDA.backlight();
    LCDA.setCursor(0, 0);
    LCDA.print("Passos: ");
    LCDA.setCursor(8, 0);  // Posicao do cursor para atualizar o contador de passos
    LCDA.print(stepCount); // atualiza o numero de passos no LCD
    LCDA.setCursor(5, 1);
    LCDA.print("Contando");
  }
  else {
    LCDA.backlight();
    LCDA.setCursor(0, 0);
    LCDA.print("Passos: ");
    LCDA.setCursor(8, 0);  // Posicao do cursor para atualizar o contador de passos
    LCDA.print(stepCount); // atualiza o numero de passos no LCD
    LCDA.setCursor(5, 1);
    LCDA.print("Pausado");
  }
}

// Funcao que calcula as calorias gastas e mostra no display
void Display_calories(){
  if(CONTADORF){
    LCDA.backlight();
    LCDA.setCursor(0, 0);
    LCDA.print("Calorias: ");
    LCDA.setCursor(10, 0);    // Posicao do cursor para atualizar o contador de passos
    LCDA.print(static_cast<int>(caloriasGastas));
    LCDA.setCursor(5, 1);
    LCDA.print("Contando");
  }
  else {
    LCDA.backlight();
    LCDA.setCursor(0, 0);
    LCDA.print("Calorias: ");
    LCDA.setCursor(10, 0);    // Posicao do cursor para atualizar o contador de passos
    LCDA.print(static_cast<int>(caloriasGastas));
    LCDA.setCursor(5, 1);
    LCDA.print("Pausado");
  }
}

bool isStep() {
  // Verifica se a amostra atual e' um pico em relacao as amostras anteriores e posteriores
  float currentSample = samples[currentIndex];
  int prevIndex = (currentIndex + windowSize - 1) % windowSize;
  int nextIndex = (currentIndex + 1) % windowSize;

  float prevSample = samples[prevIndex];
  float nextSample = samples[nextIndex];

  return currentSample > prevSample && currentSample > nextSample && currentSample > threshold;
}
#endif

