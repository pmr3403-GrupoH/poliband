#ifndef DISPLAYHORARIO_H
#define DISPLAYHORARIO_H

#include <WiFi.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

extern LiquidCrystal_I2C LCD;

void spinner();
void printLocalTime();
void timeInit();

#endif
