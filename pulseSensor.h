#ifndef PULSESENSOR_H
#define PULSESENSOR_H

#include <Arduino.h>
#include <LiquidCrystal_I2C.h>

extern LiquidCrystal_I2C lcd;

extern int pulseValue;
extern int bpm;
extern int count;
extern int sum_bpm;
extern int leitura;
extern int bpmShow;

void pulseSensorBPM();
void freq_Display();
//void printPulse();

#endif
