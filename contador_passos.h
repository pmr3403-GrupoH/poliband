#ifndef CONTADOR_PASSOS_H
#define CONTADOR_PASSOS_H

#include <Wire.h>                 // Must include Wire library for I2C
#include "SparkFun_MMA8452Q.h"    // Click here to get the library: http://librarymanager/All#SparkFun_MMA8452Q   
#include <LiquidCrystal_I2C.h>

extern LiquidCrystal_I2C LCDA;

void contador_passos_init();  // inicializa o contador de passos

void contador_passos();  // conta passos e atualiza no LCD

//void Display_passos_init(); // inicializa o LCD para mostrar contador de passos

void Display_steps();  // LCD mostra contador de passos

void Display_calories(); // calcula calorias gastas e mostra no display

bool isStep();

extern float threshold;      // Valor maximo do modulo da aceleracao para deteccao de passos
const int windowSize=50;  // Tamanho da janela de amostragem
extern int stepCount;          // variavel do contador de passos
extern float samples[windowSize];
extern int currentIndex;
extern int CONTADORF;
extern int caloriaShow;
extern float caloriasPorPasso;
extern float caloriasGastas;



#endif